#!/bin/bash

DATE=$(date +%Y%m%d)

# FIXME: delete empty layers on helper boards

DO_BACK=0
DO_FRONT=0
DO_MAIN=0
DO_UI=0

if [ $# -eq 0 ]; then
  DO_BACK=1
  DO_FRONT=1
  DO_MAIN=1
  DO_UI=1
elif [ "$1" = "back" ]; then
  DO_BACK=1
elif [ "$1" = "front" ]; then
  DO_FRONT=1
elif [ "$1" = "main" ]; then
  DO_MAIN=1
elif [ "$1" = "ui" ]; then
  DO_UI=1
else
  echo "invalid argument"
  echo "available arguments: back, front, main, ui"
  exit 1
fi

if [ $DO_BACK -eq 1 ]; then
  # separate back panel to own project
  echo "Separating back panel..."
  mkdir -p production/board_back_panel/${DATE}
  kikit separate --source 'rectangle; tlx: 60mm; tly: 14mm; brx: 240mm; bry: 75mm' JellyfishOPP.kicad_pcb production/board_back_panel/${DATE}/jellyfish_back_panel.kicad_pcb
  # fix the library path
  sed -i -r 's/\$\{KIPRJMOD\}\/lib\//\$\{KIPRJMOD\}\/..\/..\/..\/lib\//g' production/board_main/${DATE}/jellyfish_back_panel.kicad_pcb
  echo "exporting production files..."
  kikit fab jlcpcb --nametemplate "jellyfish_back_panel_{}" production/board_back_panel/${DATE}/jellyfish_back_panel.kicad_pcb production/board_back_panel/${DATE}/output
  echo "done."
fi

if [ $DO_MAIN -eq 1 ]; then
  # separate main board to own project
  echo "Separating main board..."
  mkdir -p production/board_main/${DATE}
  kikit separate --source 'rectangle; tlx: 60mm; tly: 75mm; brx: 240mm; bry: 250mm' JellyfishOPP.kicad_pcb production/board_main/${DATE}/jellyfish_main.kicad_pcb

  # fix the library path
  sed -i -r 's/\$\{KIPRJMOD\}\/lib\//\$\{KIPRJMOD\}\/..\/..\/..\/lib\//g' production/board_main/${DATE}/jellyfish_main.kicad_pcb

  #echo "exporting production files..."
  #kikit fab jlcpcb --nametemplate "jellyfish_main_{}" production/board_main/${DATE}/jellyfish_main.kicad_pcb production/board_main/${DATE}/output
  echo "done."
fi

if [ $DO_FRONT -eq 1 ]; then
  # separate front panel to own project
  echo "Separating front panel..."
  mkdir -p production/board_front_panel/${DATE}
  kikit separate --source 'rectangle; tlx: 60mm; tly: 250mm; brx: 240mm; bry: 310mm' JellyfishOPP.kicad_pcb production/board_front_panel/${DATE}/jellyfish_front_panel.kicad_pcb
  # fix the library path
  sed -i -r 's/\$\{KIPRJMOD\}\/lib\//\$\{KIPRJMOD\}\/..\/..\/..\/lib\//g' production/board_main/${DATE}/jellyfish_front_panel.kicad_pcb
  echo "exporting production files..."
  kikit fab jlcpcb --nametemplate "jellyfish_front_panel_{}" production/board_front_panel/${DATE}/jellyfish_front_panel.kicad_pcb production/board_front_panel/${DATE}/output
  echo "done."
fi

if [ $DO_UI -eq 1 ]; then
  # separate UI board to own project
  echo "Separating UI board..."
  mkdir -p production/board_ui/${DATE}
  kikit separate --source 'rectangle; tlx: 60mm; tly: 310mm; brx: 240mm; bry: 360mm' JellyfishOPP.kicad_pcb production/board_ui/${DATE}/jellyfish_ui_pcb.kicad_pcb
  # fix the library path
  sed -i -r 's/\$\{KIPRJMOD\}\/lib\//\$\{KIPRJMOD\}\/..\/..\/..\/lib\//g' production/board_main/${DATE}/jellyfish_ui_pcb.kicad_pcb
  echo "exporting production files..."
  kikit fab jlcpcb --nametemplate "jellyfish_ui_pcb_{}" production/board_ui/${DATE}/jellyfish_ui_pcb.kicad_pcb production/board_ui/${DATE}/output
  echo "done."
fi
