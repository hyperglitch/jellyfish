# JellyfishOPP hardware design files

This repository contains the hardware design files for the [JellyfishOPP](https://hyperglitch.com/jellyfish) project. The hardware design files are created using [KiCad](http://kicad-pcb.org/).

## Status

The development of the device started in October 2023. First version of the complete schematic was finished on April 10th 2024. The PCB layout is currently in progress.

## License

```
SPDX-FileCopyrightText: 2023 Igor Brkic <igor@hyperglitch.com>
SPDX-License-Identifier: CERN-OHL-S-2.0

You may redistribute and modify this source and make products using it under
the terms of the CERN-OHL-S v2 (https://ohwr.org/cern_ohl_s_v2.txt).

This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.
```
