# JellyfishOPP

JellyfishOPP is an open source, open hardware, programmable power supply and measurement device. The main page containing the description and links to all of the development updates can be found at [https://hyperglitch.com/jellyfish](https://hyperglitch.com/jellyfish).

Repository contents:
- `hw/` - Hardware design files
- `fw/` - Firmware source code
- `gw/` - Gateware source code
- `extras/` - Extra files for the project (simulations, etc.)

Check each folder for more information and license details.


## Funding

This project is funded through [NGI0 Entrust](https://nlnet.nl/entrust), a fund established by [NLnet](https://nlnet.nl) with financial support from the European Commission's [Next Generation Internet](https://ngi.eu) program. Learn more at the [NLnet project page](https://nlnet.nl/project/JellyfishOPP).

[<img src="https://nlnet.nl/logo/banner.png" alt="NLnet foundation logo" width="20%" />](https://nlnet.nl)
[<img src="https://nlnet.nl/image/logos/NGI0_tag.svg" alt="NGI Zero Logo" width="20%" />](https://nlnet.nl/entrust)
