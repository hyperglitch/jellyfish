# JellyfishOPP Firmware

This directory contains the firmware for the JellyfishOPP.

The code is generated using STM32CubeMX for the STM32F446VETx MCU.

Code is under development and currently contains only the specific tests for the peripherals.

# License

Included code is licensed under a few different licenses. Details are available here: [https://github.com/STMicroelectronics/STM32CubeF4/blob/master/LICENSE.md](https://github.com/STMicroelectronics/STM32CubeF4/blob/master/LICENSE.md)

JellyfishOPP firmware is licensed under the BSD 3-Clause License.
