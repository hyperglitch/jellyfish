/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "ST7789/st7789.h"
#include "images/splash.h"
#include <stdio.h>
#include <stdbool.h>

#include "../../USB_DEVICE/App/usbd_cdc_if.h"

#include <stdbool.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

CRC_HandleTypeDef hcrc;

I2C_HandleTypeDef hi2c3;

QSPI_HandleTypeDef hqspi;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;
DMA_HandleTypeDef hdma_spi2_tx;

TIM_HandleTypeDef htim4;

UART_HandleTypeDef huart1;

/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 1024 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* USER CODE BEGIN PV */

uint32_t adc_buff[9] = {0};     // ADC buffer
uint8_t rx_buff[2];     // UART RX buffer


// I2C peripheral addresses
const uint8_t i2c_addr_aux_pot  = 0x2c;
const uint8_t i2c_addr_pd_trig1 = 0x29;
const uint8_t i2c_addr_pd_trig2 = 0x28;
const uint8_t i2c_addr_iset_dac1= 0x60;
const uint8_t i2c_addr_iset_dac2= 0x61;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_CRC_Init(void);
static void MX_I2C3_Init(void);
static void MX_SPI1_Init(void);
static void MX_SPI2_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM4_Init(void);
static void MX_QUADSPI_Init(void);
void StartDefaultTask(void *argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

// printf support
int _write(int file, char *ptr, int len){
  (void)file;
  //HAL_UART_Transmit_IT(&huart1, (uint8_t*)ptr, (uint16_t)len);
  //CDC_Transmit_HS((uint8_t*)ptr, len);
  HAL_UART_Transmit(&huart1, (uint8_t*)ptr, (uint16_t)len, 100);

  return len;
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
  if(huart->Instance == USART1){
    // Code to handle the received data
    //handle_uart(RxBuffer[0]);
    HAL_UART_Receive_IT(&huart1, rx_buff, 1);
    printf("%c\n", rx_buff[0]); // echo
    // FIXME: set flag to process later
  }
}


typedef struct{
  uint32_t enc_sw;
  uint32_t enc_sw_evt_id;
  uint32_t btn;
  uint32_t btn_evt_id;
  int32_t encoder;
} jf_ui_t;
static jf_ui_t user_input_state = {0};
static uint32_t _enc_state[2] = {0};

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
  static uint32_t last_change = 0;
  static uint32_t enc_state_last_change = 0;
  static int32_t state_last = 0;
  static int32_t direction_count = 0;
  bool change_enc[2] = {false};
  uint32_t enc[2] = {0};
  uint32_t enc_last_change[2] = {0};
  const uint32_t enc_debounce_ms = 10;

  switch(GPIO_Pin){
  case BUTTON_Pin:
    const uint32_t btn = HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin);
    if(btn!=user_input_state.btn){
      user_input_state.btn = btn;
      user_input_state.btn_evt_id++;
    }
    return;
  case ENCODER_SW_Pin:
    const uint32_t enc_sw = HAL_GPIO_ReadPin(ENCODER_SW_GPIO_Port, ENCODER_SW_Pin);
    if(enc_sw!=user_input_state.enc_sw){
      user_input_state.enc_sw = enc_sw;
      user_input_state.enc_sw_evt_id++;
    }
    return;
    /*
  case ENCODER_A_Pin:
    enc[0] = HAL_GPIO_ReadPin(ENCODER_A_GPIO_Port, ENCODER_A_Pin);
    if(enc[0]!=_enc_state[0] && HAL_GetTick()-enc_last_change[0]>enc_debounce_ms){
      change_enc[0] = true;
      enc_last_change[0] = HAL_GetTick(); // debounce
    }
    else {
      enc[0] = _enc_state[0];
    }
    break;
  case ENCODER_B_Pin:
    enc[1] = HAL_GPIO_ReadPin(ENCODER_B_GPIO_Port, ENCODER_B_Pin);
    if(enc[1]!=_enc_state[1] && HAL_GetTick()-enc_last_change[1]>enc_debounce_ms){
      change_enc[1] = true;
      enc_last_change[1] = HAL_GetTick();
    }
    else {
      enc[1] = _enc_state[1];
    }
    break;
    */
  }

  if(GPIO_Pin!=ENCODER_A_Pin && GPIO_Pin!=ENCODER_B_Pin) return;

  const uint32_t tm = HAL_GetTick();

  if((tm-last_change) < enc_debounce_ms) return;
  last_change = tm;

  const bool A = HAL_GPIO_ReadPin(ENCODER_A_GPIO_Port, ENCODER_A_Pin);
  const bool B = HAL_GPIO_ReadPin(ENCODER_B_GPIO_Port, ENCODER_B_Pin);

  int32_t state = 0;
  if(GPIO_Pin==ENCODER_A_Pin) {
    if(B==0) state--;
    else state++;
  }
  else if(GPIO_Pin==ENCODER_B_Pin) {
    if(A==1) state--;
    else state++;
  }

  if(state==0) return;

  if(tm-enc_state_last_change > 50) { // slow down the changes

    // prevent abrupt direction change
    if(state==state_last) {
      enc_state_last_change = tm;
      user_input_state.encoder += state;
    }
    state_last = state;
  }



  /*
  if(change_enc){
    // code change: ... -> 3 -> 2 -> 0 -> 1 -> ...
    const uint8_t code_old = (_enc_state[1]<<1) + _enc_state[0];
    const uint8_t code_new = (enc[1]<<1) + enc[0];
    if((code_old==3 && code_new==2) || (code_old==2 && code_new==0) || (code_old==0 && code_new==1) || (code_old==1 && code_new==3)){
      user_input_state.encoder++;
    }
    else{
      user_input_state.encoder--;
    }
    _enc_state[0] = enc[0];
    _enc_state[1] = enc[1];
  }
  */
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_CRC_Init();
  MX_I2C3_Init();
  MX_SPI1_Init();
  MX_SPI2_Init();
  MX_USART1_UART_Init();
  MX_ADC1_Init();
  MX_TIM4_Init();
  MX_QUADSPI_Init();
  /* USER CODE BEGIN 2 */

  printf("hello from JellyfishOPP\r\n");
  printf("compiled on %s %s\r\n", __DATE__, __TIME__);

  HAL_GPIO_WritePin(DISP_BLK_GPIO_Port, DISP_BLK_Pin, 1);
  ST7789_Init();
  //ST7789_Test();
  HAL_Delay(500);

  uint16_t back = 0x0000;
  for(int i=0; i<8; i++){
    back += 0x1111;
    ST7789_Fill_Color(back);
    HAL_Delay(10);
  }
  ST7789_Fill_Color(WHITE);
  HAL_Delay(300);
  ST7789_DrawImage(0, 0, 280, 240, (uint16_t *)img_splash);
  //HAL_Delay(3000);



  // enable ideal diode controllers
  HAL_GPIO_WritePin(ENABLE_USB1_GPIO_Port, ENABLE_USB1_Pin, 1);
  HAL_GPIO_WritePin(ENABLE_DC_GPIO_Port, ENABLE_DC_Pin, 1);

  HAL_UART_Receive_IT(&huart1, rx_buff, 1);

  // initialize UI controls
  HAL_GPIO_EXTI_Callback(BUTTON_Pin);
  HAL_GPIO_EXTI_Callback(ENCODER_SW_Pin);
  HAL_GPIO_EXTI_Callback(ENCODER_A_Pin);
  HAL_GPIO_EXTI_Callback(ENCODER_B_Pin);

  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1) {


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 12;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_RCC_MCOConfig(RCC_MCO2, RCC_MCO2SOURCE_HSE, RCC_MCODIV_1);
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV8;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 9;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = 2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_2;
  sConfig.Rank = 3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = 4;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_11;
  sConfig.Rank = 5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_14;
  sConfig.Rank = 6;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_15;
  sConfig.Rank = 7;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
  sConfig.Rank = 8;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_VREFINT;
  sConfig.Rank = 9;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief CRC Initialization Function
  * @param None
  * @retval None
  */
static void MX_CRC_Init(void)
{

  /* USER CODE BEGIN CRC_Init 0 */

  /* USER CODE END CRC_Init 0 */

  /* USER CODE BEGIN CRC_Init 1 */

  /* USER CODE END CRC_Init 1 */
  hcrc.Instance = CRC;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CRC_Init 2 */

  /* USER CODE END CRC_Init 2 */

}

/**
  * @brief I2C3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C3_Init(void)
{

  /* USER CODE BEGIN I2C3_Init 0 */

  /* USER CODE END I2C3_Init 0 */

  /* USER CODE BEGIN I2C3_Init 1 */

  /* USER CODE END I2C3_Init 1 */
  hi2c3.Instance = I2C3;
  hi2c3.Init.ClockSpeed = 50000;
  hi2c3.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c3.Init.OwnAddress1 = 0;
  hi2c3.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c3.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c3.Init.OwnAddress2 = 0;
  hi2c3.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c3.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C3_Init 2 */

  /* USER CODE END I2C3_Init 2 */

}

/**
  * @brief QUADSPI Initialization Function
  * @param None
  * @retval None
  */
static void MX_QUADSPI_Init(void)
{

  /* USER CODE BEGIN QUADSPI_Init 0 */

  /* USER CODE END QUADSPI_Init 0 */

  /* USER CODE BEGIN QUADSPI_Init 1 */

  /* USER CODE END QUADSPI_Init 1 */
  /* QUADSPI parameter configuration*/
  hqspi.Instance = QUADSPI;
  hqspi.Init.ClockPrescaler = 9;
  hqspi.Init.FifoThreshold = 1;
  hqspi.Init.SampleShifting = QSPI_SAMPLE_SHIFTING_NONE;
  hqspi.Init.FlashSize = 20;
  hqspi.Init.ChipSelectHighTime = QSPI_CS_HIGH_TIME_1_CYCLE;
  hqspi.Init.ClockMode = QSPI_CLOCK_MODE_0;
  hqspi.Init.DualFlash = QSPI_DUALFLASH_ENABLE;
  if (HAL_QSPI_Init(&hqspi) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN QUADSPI_Init 2 */

  /* USER CODE END QUADSPI_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 0;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 65535;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 1024000;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_8;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream4_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream4_IRQn);
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, DISP_BLK_Pin|DISP_RES_Pin|DISP_DC_Pin|ENABLE_PREREG_Pin
                          |ENABLE_EXT_IN_Pin|ENABLE_AUX_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, ENABLE_USB1_Pin|ENABLE_ISO_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : DISP_BLK_Pin DISP_RES_Pin DISP_DC_Pin ENABLE_PREREG_Pin
                           ENABLE_EXT_IN_Pin ENABLE_AUX_Pin */
  GPIO_InitStruct.Pin = DISP_BLK_Pin|DISP_RES_Pin|DISP_DC_Pin|ENABLE_PREREG_Pin
                          |ENABLE_EXT_IN_Pin|ENABLE_AUX_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : BUTTON_Pin */
  GPIO_InitStruct.Pin = BUTTON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(BUTTON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : ENCODER_A_Pin ENCODER_B_Pin ENCODER_SW_Pin */
  GPIO_InitStruct.Pin = ENCODER_A_Pin|ENCODER_B_Pin|ENCODER_SW_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PC9 */
  GPIO_InitStruct.Pin = GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF0_MCO;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : ENABLE_DC_Pin */
  GPIO_InitStruct.Pin = ENABLE_DC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ENABLE_DC_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : ENABLE_USB1_Pin ENABLE_ISO_Pin */
  GPIO_InitStruct.Pin = ENABLE_USB1_Pin|ENABLE_ISO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

bool vaux_set_digipot(uint8_t val){
  uint8_t wiper_data[2] = {(val>>8)&0x01, val&0xff};
  const HAL_StatusTypeDef st = HAL_I2C_Master_Transmit(&hi2c3, i2c_addr_aux_pot<<1, wiper_data, 2, 300);
  return st==HAL_OK;
}


typedef enum{
  DAC_ISET_PLUS,
  DAC_ISET_MINUS
} iset_dac_idx_t;

bool iset_dac_write(iset_dac_idx_t dac_idx, uint16_t val){
  const uint8_t addr = dac_idx==DAC_ISET_PLUS ? i2c_addr_iset_dac1 : i2c_addr_iset_dac2;
  const uint8_t MCP4725_DAC_WRITE_CMD = 0x40;
  const uint8_t MCP4725_DAC_EEPROM_WRITE_CMD = 0x60;
  uint8_t data[3] = {MCP4725_DAC_WRITE_CMD, (uint8_t)(val>>4), (val<<4)&0xff};
  const HAL_StatusTypeDef st = HAL_I2C_Master_Transmit(&hi2c3, addr<<1, data, 3, 300);
  return st==HAL_OK;
}





HAL_StatusTypeDef QSPI_Write(uint8_t* data, uint8_t command, uint32_t address, uint16_t dataSize) {
  QSPI_CommandTypeDef sCommand;

  // Configure the command for the write operation
  sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
  sCommand.Instruction = command; //0x02;  // Page Program instruction (example)
  sCommand.AddressMode = QSPI_ADDRESS_4_LINES;
  sCommand.AddressSize = QSPI_ADDRESS_24_BITS;
  sCommand.Address = address;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  sCommand.DataMode = QSPI_DATA_4_LINES;
  sCommand.NbData = dataSize;
  sCommand.DummyCycles = 2;
  sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

  // Send the command
  if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
    return HAL_ERROR;
  }

  // Transmit the data
  if (HAL_QSPI_Transmit(&hqspi, data, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
    return HAL_ERROR;
  }

  return HAL_OK;
}


HAL_StatusTypeDef QSPI_Read(uint8_t* data, uint8_t command,uint32_t address, uint16_t dataSize) {
    QSPI_CommandTypeDef sCommand;

    // Configure the command for the read operation
    sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    sCommand.Instruction = command; //0x03;  // Read Data instruction (example)
    sCommand.AddressMode = QSPI_ADDRESS_4_LINES;
    sCommand.AddressSize = QSPI_ADDRESS_24_BITS;
    sCommand.Address = address;
    sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    sCommand.DataMode = QSPI_DATA_4_LINES;
    sCommand.NbData = dataSize;
    sCommand.DummyCycles = 2; // QSPI_DUMMY_CYCLES_READ
    sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
    sCommand.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    // Send the command
    if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return HAL_ERROR;
    }

    // Receive the data
    if (HAL_QSPI_Receive(&hqspi, data, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return HAL_ERROR;
    }

    return HAL_OK;
}


typedef enum {
  LED_BLINK,
  ADC_GET,
  DAC_SET,
  ISET_DAC,
  RANGE,
  UI_FUNC_END, // FIXME: must be last
} ui_function_t;

const char ui_func_labels[13][13] = {
  "LED    ",
  "ADC_GET",
  "DAC    ",
  "ISET   ",
  "RANGE  ",
};

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 5 */


  /*
  printf("I2c scan\r\n");
  osDelay(50);
  for(int i=0; i<0x80; i++)  {
    if(i%16==0){
      printf("\r\n 0x%02X: ", i);
    }

    uint8_t ret = HAL_I2C_IsDeviceReady(&hi2c3, (uint16_t)(i<<1), 10, 100);
    if (ret != HAL_OK) // No ACK Received At That Address
    {
      printf("  -- ");
    }
    else if(ret == HAL_OK)
    {
      printf(" 0x%02X", i);
    }
    else{
      printf("  ?? ");
    }
    osDelay(5);
  }
  printf("\r\n");
  */


/*
  while(true){
    uint8_t rx_buff[2];
    HAL_I2C_Mem_Read(&hi2c3, 0x60<<1, 1, 1, rx_buff, 1, 300);
    printf("r: %x\r\n", rx_buff[0]);
    osDelay(500);
  }
*/


  const uint32_t ch[3] = {TIM_CHANNEL_1, TIM_CHANNEL_2, TIM_CHANNEL_3};
  int curr = 0;
  int32_t p = htim4.Init.Period/4;
  for(int i=0; i<3; i++){
      __HAL_TIM_SET_COMPARE(&htim4, ch[i], 0);
      HAL_TIM_PWM_Start(&htim4,  ch[i]);
  }

  uint32_t last_adc_trigger = 0;

  uint32_t btn_last = 1;
  uint32_t enc_sw_last = 1;
  int32_t enc_last = user_input_state.encoder;

  ui_function_t ui_func = LED_BLINK;

  uint16_t iset_dac = 0;

  bool refresh_display = true;

  uint32_t spi_addr = 0x0001;


  HAL_GPIO_WritePin(ENABLE_PREREG_GPIO_Port, ENABLE_PREREG_Pin, 1);
  ST7789_WriteString(5, 20, " PREREG ENABLED ", Font_16x26, RED, WHITE);

  iset_dac_write(DAC_ISET_PLUS, 100);
  iset_dac_write(DAC_ISET_MINUS, 100);

  // set BK2 IO pins as input (E7, E8, E9, E10)
  // this is to avoid the possible conflict with FPGA pins set to output
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  GPIO_InitStruct.Pin = GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10;;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  //HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /* Infinite loop */
  for(;;){
    /*
    if(HAL_GetTick()-last_adc_trigger > 1000){
      last_adc_trigger = HAL_GetTick();
      // print last values
      printf("-----------------------\r\n");
      const char labels[13][13] = {
          "VSENSE_DC  ",
          "VSENSE_USB0",
          "VSENSE_USB1",
          "NTC1  ",
          "NTC2  ",
          "AUX_ISENSE ",
          "AUX_VSENSE ",
          "TEMPERATURE",
          "VREFINT    ",
      };
      for(int i=0; i<9; i++){
        printf("  %s: %lu\r\n", labels[i], adc_buff[i]);
      }
      HAL_ADC_Start_DMA(&hadc1, adc_buff, 9);
    }
    */

    /*
    // sawtooth
    iset_dac_write(DAC_ISET_PLUS, iset_dac);
    iset_dac_write(DAC_ISET_MINUS, 4095-iset_dac);
    iset_dac+=10;
    if(iset_dac>4095) iset_dac = 0;
    */


    __HAL_TIM_SET_COMPARE(&htim4, ch[curr], p);
    p-=100;
    if(p<=0){
        p = htim4.Init.Period/4;
        curr++;
        if(curr==3) curr = 0;
    }
    osDelay(10);

    const uint32_t enc_sw = user_input_state.enc_sw;
    if(enc_sw!=enc_sw_last) {
      enc_sw_last = enc_sw;
      if(!enc_sw) {
        const uint8_t spi_data[16] = {
          0xc3, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
        };

        int32_t d = user_input_state.encoder;
        if(d>99) d = 99;
        if(d<0) d = 0;

        if(ui_func==LED_BLINK) {
          const uint8_t cmd = 0x05;
          const uint32_t addr = d*2;
          QSPI_Write(spi_data, cmd, addr, 1);
          printf("send LED_BLINK command %d with addr %d\r\n", cmd, addr);
        }
        else if(ui_func==DAC_SET) {
          const uint8_t cmd = 0x06;
          const uint32_t addr = d*64;
          QSPI_Write(spi_data, cmd, addr, 1);
          printf("send DAC_SET command %d with addr %d\r\n", cmd, addr);
        }
        else if(ui_func==ISET_DAC) {
          const uint16_t val = d*10;
          iset_dac_write(DAC_ISET_PLUS, val);
          iset_dac_write(DAC_ISET_MINUS, val);
          printf("send ISET_DAC command value %d\r\n", val);
        }
        else if(ui_func==RANGE) {
          if(d>6) d = 6;
          const int8_t cmd = 0x04;
          const uint32_t addr = d*2;
          QSPI_Write(spi_data, cmd, addr, 1);
          printf("send RANGE command %d with addr %d\r\n", cmd, addr);
        }
        else if(ui_func==ADC_GET) {
          const uint32_t addr = d*2;
          QSPI_Read(spi_data, 0x01, addr, 16);
          printf("qspi read %x %x %x %x %x %x %x %x\r\n",
            spi_data[0], spi_data[1], spi_data[2], spi_data[3], spi_data[4], spi_data[5], spi_data[6], spi_data[7]);
        }
      }
    }

    const uint32_t btn = user_input_state.btn;
    if(btn!=btn_last) {
      btn_last = btn;
      if(!btn) {
        ui_func++;
        if(ui_func==UI_FUNC_END) ui_func = LED_BLINK;
        printf("ui_func: %s\r\n", ui_func_labels[ui_func]);
        refresh_display = true;

        // toggle ext in
        //HAL_GPIO_TogglePin(ENABLE_EXT_IN_GPIO_Port, ENABLE_EXT_IN_Pin);
      }
    }

    /*
    if(btn!=btn_last){
      btn_last = btn;
      //printf("button state: %lu\r\n", btn);
      if(btn==0){
        // toggle AUX supply
        const uint32_t st = HAL_GPIO_ReadPin(ENABLE_PREREG_GPIO_Port, ENABLE_PREREG_Pin);
        if(st){
          printf("Disabling PREREG\r\n");
          HAL_GPIO_WritePin(ENABLE_PREREG_GPIO_Port, ENABLE_PREREG_Pin, 0);
        }
        else{
          printf("Enabling PREREG\r\n");
          HAL_GPIO_WritePin(ENABLE_PREREG_GPIO_Port, ENABLE_PREREG_Pin, 1);
        }
      }
    }
    */
    /*
    const uint32_t enc_sw = user_input_state.enc_sw;
    if(enc_sw!=enc_sw_last){
      enc_sw_last = enc_sw;
      //printf("enc_sw state: %lu\r\n", enc_sw);

      if(enc_sw==0){
        // toggle AUX supply
        const uint32_t st = HAL_GPIO_ReadPin(ENABLE_AUX_GPIO_Port, ENABLE_AUX_Pin);
        if(st){
          printf("Disabling AUX supply\r\n");
          HAL_GPIO_WritePin(ENABLE_AUX_GPIO_Port, ENABLE_AUX_Pin, 0);
        }
        else{
          printf("Enabling AUX supply\r\n");
          HAL_GPIO_WritePin(ENABLE_AUX_GPIO_Port, ENABLE_AUX_Pin, 1);
        }
      }
    }
    */
    int32_t enc = user_input_state.encoder;
    if(enc!=enc_last) {
      enc_last = enc;
      refresh_display = true;
      // set digipot wiper
      if(enc>=0 && enc<=255){
        /*
        vaux_set_digipot(enc);
        iset_dac_write(DAC_ISET_PLUS, enc*16);
        iset_dac_write(DAC_ISET_MINUS, enc*16);

        printf("wiper set to %ld\r\n", enc);
        */
      }
      else{
        printf("encoder: %ld\r\n", enc);
      }
    }

    if(refresh_display){
      refresh_display = false;
      char buf[10];
      ST7789_WriteString(110, 150, HAL_GPIO_ReadPin(ENABLE_EXT_IN_GPIO_Port, ENABLE_EXT_IN_Pin) ? "EXT IN":"      ", Font_16x26, GREEN, WHITE);

      ST7789_WriteString(60, 175, ui_func_labels[ui_func], Font_16x26, BLUE, WHITE);
      ST7789_WriteString(60, 200, "enc:", Font_16x26, BLACK, WHITE);

      ST7789_WriteString(130, 200, "    ", Font_16x26, BLACK, WHITE); // clear
      ST7789_WriteString(130, 200, itoa(enc, buf, 10), Font_16x26, BLACK, WHITE);

      ST7789_WriteString(130, 200, "    ", Font_16x26, BLACK, WHITE); // clear
      ST7789_WriteString(130, 200, itoa(enc, buf, 10), Font_16x26, BLACK, WHITE);
    }

    /*
    if(btn) {
      uint8_t spi_data[16] = {0};
      QSPI_Read(spi_data, 0xAD54, sizeof(spi_data));
    }
    else {
      const uint8_t spi_data[16] = {
        0xc3, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
      };
      QSPI_Write(spi_data, 0xAD54, sizeof(spi_data));
    }
    */


  }
  /* USER CODE END 5 */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
