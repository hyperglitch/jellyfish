#ifndef __SPLASH_IMG_H__
#define __SPLASH_IMG_H__

#include <stdint.h>

extern const uint16_t img_splash[67200];
extern const uint16_t img_splash_inv[67200];


#endif
