/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

#define CLI_NAME JellyfishOPP

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DISP_BLK_Pin GPIO_PIN_3
#define DISP_BLK_GPIO_Port GPIOE
#define DISP_RES_Pin GPIO_PIN_4
#define DISP_RES_GPIO_Port GPIOE
#define DISP_DC_Pin GPIO_PIN_5
#define DISP_DC_GPIO_Port GPIOE
#define BUTTON_Pin GPIO_PIN_6
#define BUTTON_GPIO_Port GPIOE
#define BUTTON_EXTI_IRQn EXTI9_5_IRQn
#define NTC_2_Pin GPIO_PIN_1
#define NTC_2_GPIO_Port GPIOC
#define VSENSE_DC_Pin GPIO_PIN_0
#define VSENSE_DC_GPIO_Port GPIOA
#define VSENSE_USB0_Pin GPIO_PIN_1
#define VSENSE_USB0_GPIO_Port GPIOA
#define VSENSE_USB1_Pin GPIO_PIN_2
#define VSENSE_USB1_GPIO_Port GPIOA
#define NTC_1_Pin GPIO_PIN_4
#define NTC_1_GPIO_Port GPIOA
#define ISENSE_AUX_Pin GPIO_PIN_4
#define ISENSE_AUX_GPIO_Port GPIOC
#define VSENSE_AUX_Pin GPIO_PIN_5
#define VSENSE_AUX_GPIO_Port GPIOC
#define ENABLE_PREREG_Pin GPIO_PIN_11
#define ENABLE_PREREG_GPIO_Port GPIOE
#define ENABLE_EXT_IN_Pin GPIO_PIN_13
#define ENABLE_EXT_IN_GPIO_Port GPIOE
#define ENABLE_AUX_Pin GPIO_PIN_14
#define ENABLE_AUX_GPIO_Port GPIOE
#define DISP_MOSI_Pin GPIO_PIN_15
#define DISP_MOSI_GPIO_Port GPIOB
#define ENCODER_A_Pin GPIO_PIN_8
#define ENCODER_A_GPIO_Port GPIOD
#define ENCODER_A_EXTI_IRQn EXTI9_5_IRQn
#define ENCODER_B_Pin GPIO_PIN_9
#define ENCODER_B_GPIO_Port GPIOD
#define ENCODER_B_EXTI_IRQn EXTI9_5_IRQn
#define ENCODER_SW_Pin GPIO_PIN_10
#define ENCODER_SW_GPIO_Port GPIOD
#define ENCODER_SW_EXTI_IRQn EXTI15_10_IRQn
#define DISP_SCL_Pin GPIO_PIN_7
#define DISP_SCL_GPIO_Port GPIOC
#define DISP_NCS_Pin GPIO_PIN_1
#define DISP_NCS_GPIO_Port GPIOD
#define ENABLE_DC_Pin GPIO_PIN_4
#define ENABLE_DC_GPIO_Port GPIOD
#define ENABLE_USB1_Pin GPIO_PIN_5
#define ENABLE_USB1_GPIO_Port GPIOD
#define ENABLE_ISO_Pin GPIO_PIN_6
#define ENABLE_ISO_GPIO_Port GPIOD

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
