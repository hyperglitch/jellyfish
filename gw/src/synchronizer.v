// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module synchronizer(
  input wire async_signal,     // Fast asynchronous input signal
  input wire clk,              // FPGA clock (slower clock domain)
  output reg rising_edge      // Rising edge detected
);

  // Registers for edge detection
  reg async_prev;              // Previous state of async_signal
  reg async_rising;            // Rising edge detected in async domain

  // Process for edge detection (single-edge sensitive)
  always @(posedge async_signal) begin
    async_rising <= 1'b1;      // Detect rising edge
  end

  // Update previous state of async_signal in a separate block
  always @(posedge clk) begin
    async_prev <= async_signal; // Track previous value in async domain
  end

  // Synchronize rising and falling edges to FPGA clock
  reg sync_rising_0, sync_rising_1;

  always @(posedge clk) begin
    // Synchronize rising edge
    sync_rising_0 <= async_rising;
    sync_rising_1 <= sync_rising_0;
    if (sync_rising_1) begin
      rising_edge <= 1'b1;
      async_rising <= 1'b0;   // Clear rising edge latch
    end else begin
      rising_edge <= 1'b0;
    end

  end

endmodule
