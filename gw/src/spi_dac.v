// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module spi_dac(
  input i_clk,
  input i_rst,
  input [15:0] i_data,
  input i_trigger,
  output ncs,
  output sdi,
  output sck
);

  reg [$clog2(24)-1:0] counter = 0;
  reg [23:0] data = 0;
  reg [15:0] data16;

  reg o_ncs = 1'b1;
  reg o_sdi = 1'b0;
  reg o_sck = 1'b0;

  assign ncs = o_ncs;
  assign sdi = o_sdi;
  assign sck = o_sck;

  //always @(posedge i_clk or posedge i_trigger) begin
  always @(posedge i_clk) begin
    if (i_trigger == 1'b1) begin
      // trigger happened, copy the data to the internal register
      // and set everything for sending
      o_sdi <= 1'b0;
      o_ncs <= 1'b0;
      o_sck <= 1'b1;

      data[23:16] <= 8'h08;  // register address
      //data[15:0] <= i_data;  // DAC value
      data[15:0] <= 16'hf0f0;  // DAC value

      // set the first bit immediately
      // (it's manually set to 0 as the data register is not yet ready)
      o_sdi <= 1'b0; // first bit of register address
      counter <= 1; // immediately increase the counter

    end else begin
      // if the NCS line is low, we are sending data
      if (o_ncs == 1'b0) begin
        if (counter == 24) begin
          // sent all bits, stop sending
          // first set the clock to low and then deselect the chip in the next clock cycle
          if (o_sck == 1'b1) begin
            o_sck <= 1'b0; // first drop the clock
          end else begin
            o_ncs <= 1'b1; // then deselect the chip
            o_sck <= 1'b1; // and set the clock to high (ready to send the next byte)
          end

        end else begin
          // if the clock is high set the data bit
          // otherwise drop the clock

          if (o_sck == 1'b0) begin
            // set the data bit
            o_sdi <= data[23-counter];
            counter <= counter + 1;
          end

          o_sck <= ~o_sck; // toggle the clock

        end
      end
    end
  end

endmodule
