// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module latch #(
  parameter integer DURATION = 1000000
)(
  input i_clk,
  input i_rst,
  input i_trigger,
  output reg o_signal
);

  reg [$clog2(DURATION)-1:0] duration_counter = 0;

  localparam [1:0] STATE_IDLE = 2'd0;
  localparam [1:0] STATE_LATCH = 2'd1;

  reg [1:0] state = STATE_IDLE;

  always @(posedge i_clk or posedge i_rst) begin
    if (i_rst) begin
      state <= STATE_IDLE;
      duration_counter <= 0;
      o_signal <= 1'b0;

    end else begin
      case (state)

        STATE_IDLE: begin
          if (i_trigger == 1'b1) begin
            state <= STATE_LATCH;
            duration_counter <= 0;
            o_signal <= 1'b1;
          end
        end

        STATE_LATCH: begin
          duration_counter <= duration_counter + 1;
          if (duration_counter == DURATION-1) begin
            state <= STATE_IDLE;
            o_signal <= 1'b0;
          end
        end

        default: begin
          state <= STATE_IDLE;
        end
      endcase
    end
  end

endmodule
