// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module bram
(
  input clk,
  input [10:0] addr,
  output reg [15:0] data
);

  SB_RAM40_4K #(
    .INIT_0(256'h0123_4567_89AB_CDEF_0123_456789_ABCDEF_0123456789ABCDEF_0123456789ABCDEF),
    .INIT_1(256'h1111111122222222_3333333344444444_5555555566666666_7777777788888888),
    .INIT_2(256'hAAAAAAAABBBBBBBB_CCCCCCCCDDDDDDDD_EEEEEEEEFFFFFFFF_0000000099999999),
    .INIT_3(256'hCAFEBABEDEADBEEF_FEEDFACEBADC0FFE_1234567890ABCDEF_13579BDF2468ACEE),
    .INIT_4(256'h0),
    .INIT_5(256'h0),
    .INIT_6(256'h0),
    .INIT_7(256'h0)
  ) bram_inst (
    .RDATA(data),
    .RADDR(addr),
    .RCLK(clk),
    .RCLKE(1'b1),
    .RE(1'b1),
    .WADDR(11'b0), // writing disabled
    .WCLK(clk),
    .WCLKE(1'b1),
    .WE(1'b0),
    .MASK(16'b0)  // no masking
  );


endmodule
