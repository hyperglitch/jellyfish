// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module shift_register
#(
  parameter integer WIDTH = 8,
  parameter integer SHIFT_LEFT = 1
)(
  input i_clk,
  input i_rst,
  input i_input,
  output reg [WIDTH-1:0] o_data
);

  always @(posedge i_clk or posedge i_rst) begin
    if (i_rst) begin
      o_data <= 0;
    end else begin
      if (SHIFT_LEFT) begin
        o_data <= {o_data[WIDTH-2:0], i_input};
      end else begin
        o_data <= {i_input, o_data[WIDTH-1:1]};
      end
    end
  end

endmodule
