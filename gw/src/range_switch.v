// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module range_switch(
  input [2:0] i_range,
  input i_trigger,
  input i_clk,
  output o_meas_switch_0,
  output o_meas_switch_1,
  output o_meas_switch_2,
  output o_range_sel_0,
  output o_range_sel_1,
  output o_range_sel_2,
  output o_range_sel_3,
  output o_range_sel_4,
  output o_range_sel_5
);

  reg [2:0] range = 3'd0;
  reg trigger_old = 1'b0;

  always @(posedge i_clk) begin
    trigger_old <= i_trigger;

    if (i_trigger == 1'b1 && trigger_old == 1'b0) begin
      range <= i_range;
    end
  end

  // range select, value 6 disables all switches
  assign o_range_sel_0 =  range==3'd0 ? 1'b1 : 1'b0;
  assign o_range_sel_1 =  range==3'd1 ? 1'b1 : 1'b0;
  assign o_range_sel_2 =  range==3'd2 ? 1'b1 : 1'b0;
  assign o_range_sel_3 =  range==3'd3 ? 1'b1 : 1'b0;
  assign o_range_sel_4 =  range==3'd4 ? 1'b1 : 1'b0;
  assign o_range_sel_5 =  range==3'd5 ? 1'b1 : 1'b0;


  //                                                      s2 s1 s0
  // range 0, not used ,     MUX_IN_RANGE_0, S5, sw[2:0] = 1  0  0
  // range 1, 0.1R resistor, MUX_IN_RANGE_1, S5, sw[2:0] = 1  0  0
  // range 2, 1.0R resistor, MUX_IN_RANGE_2, S6, sw[2:0] = 1  0  1
  // range 3, 10R resistor,  MUX_IN_RANGE_3, S7, sw[2:0] = 1  1  0
  // range 4, 100R resistor, MUX_IN_RANGE_4, S8, sw[2:0] = 1  1  1
  // range 5, 1000R resistor,MUX_IN_RANGE_5, S1, sw[2:0] = 0  0  0

  assign o_meas_switch_0 = range==3'd2 || range==3'd4 ? 1'b1 : 1'b0;
  assign o_meas_switch_1 = range==3'd3 || range==3'd4 ? 1'b1 : 1'b0;
  assign o_meas_switch_2 = range==3'd0 || range==3'd1 || range==3'd2 || range==3'd3 || range==3'd4 ? 1'b1 : 1'b0;

endmodule

