// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module clock_divider
#(parameter
  integer DIVISOR = 1
)(
  input i_rst,
  input i_clk,
  output reg o_clk
);

  reg [$clog2((DIVISOR>2) ? DIVISOR : 2)-1:0] count;

  always @(posedge i_clk or posedge i_rst) begin
    if (i_rst) begin
      // reset the state
      count <= 0;
      o_clk <= 0;
    end else begin
      // otherwise, divide
      if(DIVISOR==1) begin
        o_clk <= i_clk;
      end else if(count == (DIVISOR/2-1)) begin
        o_clk <= ~o_clk;
        count <= 0;
      end else begin
        count <= count + 1;
      end
    end
  end

  always @(negedge i_clk) begin
    if(DIVISOR==1) begin
      o_clk <= i_clk;
    end
  end

endmodule

