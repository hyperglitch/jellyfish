// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module number_blinker
#(
  parameter integer PERIOD = 100,
  parameter integer GAP_PERIOD = 1000
)(
  input [7:0] i_number,
  input i_clk,
  input i_rst,
  input i_trigger,
  output reg o_signal
);


  // states
  localparam [1:0] IDLE = 2'd0;
  localparam [1:0] BLINK_HIGH = 2'd1;
  localparam [1:0] BLINK_LOW = 2'd2;
  localparam [1:0] GAP = 2'd3;

  reg [1:0] current_state = IDLE;
  reg [7:0] current_number = 0;

  // counter for clock cycles
  reg [$clog2(PERIOD)-1:0] cycle_counter = 0;

  // counter for the number of blinks
  reg [7:0] blink_counter = 0;

  // counter for the gap period
  reg [$clog2(GAP_PERIOD)-1:0] gap_counter = 0;

  reg trigger_old = 1'b0;

  // state update and transition logic
  always @(posedge i_clk or posedge i_rst) begin
    if (i_rst) begin
      // reset all registers
      current_state <= IDLE;
      o_signal <= 0;
      cycle_counter <= 0;
      blink_counter <= 0;
      gap_counter <= 0;
    end else begin
      trigger_old <= i_trigger;

      if (i_trigger == 1'b1 && trigger_old == 1'b0) begin
        current_number <= i_number;
        current_state <= IDLE;
      end

      case (current_state)

        IDLE: begin
          o_signal <= 0;
          cycle_counter <= 0;
          blink_counter <= 0;
          gap_counter <= 0;

          if (current_number == 0) begin
            current_state <= GAP;
          end else begin
            current_state <= BLINK_HIGH;
          end
        end

        BLINK_HIGH: begin
          o_signal <= 1;
          if (cycle_counter == PERIOD-1) begin
            cycle_counter <= 0;
            current_state <= BLINK_LOW;
          end else begin
            cycle_counter <= cycle_counter + 1;
          end
        end

        BLINK_LOW: begin
          o_signal <= 0;
          if (cycle_counter == PERIOD-1) begin
            cycle_counter <= 0;

            if (blink_counter == current_number-1) begin
              blink_counter <= 0;
              current_state <= GAP;
            end else begin
              blink_counter <= blink_counter + 1;
              current_state <= BLINK_HIGH;
            end
          end else begin
            cycle_counter <= cycle_counter + 1;
          end
        end

        GAP: begin
          o_signal <= 0;
          if (gap_counter == GAP_PERIOD-2) begin  // compensate for extra clock cycle in IDLE
            current_state <= IDLE;
          end else begin
            gap_counter <= gap_counter + 1;
          end
        end

        default: begin
          // should never happen, in case of invalid state
          o_signal <= 0;
          current_state <= IDLE;
        end

      endcase
    end
  end


endmodule
