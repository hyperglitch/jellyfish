// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module top(                     //top module
  input CLK,
  output LED1,

  input QSPI_CLK,
  input QSPI_NCS,

  inout QSPI_BK1_IO0,
  inout QSPI_BK1_IO1,
  inout QSPI_BK1_IO2,
  inout QSPI_BK1_IO3,

  inout QSPI_BK2_IO0,
  inout QSPI_BK2_IO1,
  inout QSPI_BK2_IO2,
  inout QSPI_BK2_IO3,

  output DAC_NCS,
  output DAC_SDI,
  output DAC_SCK,

  output MEAS_SWITCH_0,
  output MEAS_SWITCH_1,
  output MEAS_SWITCH_2,
  output RANGE_SEL_0,
  output RANGE_SEL_1,
  output RANGE_SEL_2,
  output RANGE_SEL_3,
  output RANGE_SEL_4,
  output RANGE_SEL_5,

  output ADC_NCS1,
  output ADC_NCS2,
  output ADC_SDI,
  input ADC_SDO0,
  input ADC_SDO1,
  input ADC_SDO2,
  input ADC_SDO3,
  input ADC_SDO4,
  input ADC_SDO5,
  input ADC_SDO6,
  input ADC_SDO7,
  output ADC_SCK,
  output ADC_NRST,
  output ADC_CONVST,
  input ADC_RDY1,
  input ADC_RDY2,
  output ADC_MUX_SW
);

  // initial reset signal after power on
  localparam RESET_DURATION_TICKS = 16'd2;

  // QSPI interface parameters
  localparam QSPI_ADDRESS_WIDTH = 24;
  localparam QSPI_ADDRESS_LINES = 4;
  localparam QSPI_COMMAND_WIDTH = 8;
  localparam QSPI_COMMAND_LINES = 1;
  localparam QSPI_DUMMY_CYCLES = 2;
  localparam QSPI_DATA_WIDTH = 8;
  localparam QSPI_DATA_LINES = 4;
  localparam QSPI_DATA_BANKS = 2;

  // main clock
  //wire clk_in = CLK;
  wire clk_main = CLK;
  //wire clk_aux;


  // power on reset
  reg [15:0] reset_counter = RESET_DURATION_TICKS;
  wire rst = (reset_counter != 16'd0);
  always @(posedge CLK) begin
    if (reset_counter != 16'd0) begin
      reset_counter <= reset_counter - 16'd1;
    end
  end


  wire [10:0] ram_addr = qspi_address[15:0];
  wire [10:0] ram_next_addr;
  wire [15:0] ram_data;

  bram ram (
    .clk(clk_main),
    .addr(ram_addr),
    .data(ram_data)
  );

  always @(posedge clk_main) begin
    //ram_addr <= ram_next_addr;
  end

  /*
  // divide the clock by 4
  // output is wired to clk_main
  clock_divider #(.DIVISOR(1)) cd (
    .i_rst(rst),
    .i_clk(clk_main),
    .o_clk(clk_aux)
  );
  */


  // ADC interface setup
  // -----------------------------------------------------------------------------

  wire adc_trigger;

  adc_ctrl adc (
    .i_rst(rst),
    .i_clk(clk_main),
    .o_ncs1(ADC_NCS1),
    .o_ncs2(ADC_NCS2),
    .o_sdi(ADC_SDI),
    .i_sdo0(ADC_SDO0),
    .i_sdo1(ADC_SDO1),
    .i_sdo2(ADC_SDO2),
    .i_sdo3(ADC_SDO3),
    .i_sdo4(ADC_SDO4),
    .i_sdo5(ADC_SDO5),
    .i_sdo6(ADC_SDO6),
    .i_sdo7(ADC_SDO7),
    .o_sck(ADC_SCK),
    .o_nrst(ADC_NRST),
    .o_convst(ADC_CONVST),
    .i_rdy1(ADC_RDY1),
    .i_rdy2(ADC_RDY2),
    .o_mux_sw(ADC_MUX_SW),
    .i_trigger(adc_trigger)
  );
  // /ADC



  // QSPI interface setup
  // -----------------------------------------------------------------------------
  wire qspi_addr_rdy;
  wire qspi_cmd_rdy;
  wire qspi_data_rdy;
  wire qspi_data_done;
  reg qspi_data_done_old = 1'b0;

  localparam [1:0] QSPI_IDLE      = 2'd0;
  localparam [2:0] QSPI_CMD_READ  = 2'd1;
  localparam [2:0] QSPI_ADDR_READ = 2'd2;
  localparam [2:0] QSPI_DATA_READ = 2'd3;

  wire [QSPI_ADDRESS_WIDTH-1:0] qspi_address;
  wire [QSPI_COMMAND_WIDTH-1:0] qspi_command;
  wire [QSPI_DATA_WIDTH*QSPI_DATA_BANKS-1:0] qspi_data;

  reg [QSPI_DATA_WIDTH*QSPI_DATA_BANKS-1:0] qspi_data_in = 16'h1234;

  qspi_decoder #(
    .ADDRESS_WIDTH(QSPI_ADDRESS_WIDTH),
    .ADDRESS_LINES(QSPI_ADDRESS_LINES),
    .COMMAND_WIDTH(QSPI_COMMAND_WIDTH),
    .COMMAND_LINES(QSPI_COMMAND_LINES),
    .DUMMY_CYCLES(QSPI_DUMMY_CYCLES),
    .DATA_WIDTH(QSPI_DATA_WIDTH),
    .DATA_LINES(QSPI_DATA_LINES),
    .DATA_BANKS(QSPI_DATA_BANKS)
  ) _qspi_decoder (
    .i_rst(rst),
    .i_ncs(QSPI_NCS),
    .i_main_clk(clk_main),
    .i_clk(QSPI_CLK),
    .i_bk1_io0(QSPI_BK1_IO0),
    .i_bk1_io1(QSPI_BK1_IO1),
    .i_bk1_io2(QSPI_BK1_IO2),
    .i_bk1_io3(QSPI_BK1_IO3),
    .i_bk2_io0(QSPI_BK2_IO0),
    .i_bk2_io1(QSPI_BK2_IO1),
    .i_bk2_io2(QSPI_BK2_IO2),
    .i_bk2_io3(QSPI_BK2_IO3),
    .o_cmd_rdy(qspi_cmd_rdy),
    .o_addr_rdy(qspi_addr_rdy),
    .o_data_rdy(qspi_data_rdy),
    .o_address(qspi_address),
    .o_command(qspi_command),
    .o_data(qspi_data),
    .i_data(qspi_data_in),
    .o_data_done(qspi_data_done),

    .i_ram_data(ram_data),
    .o_ram_next_addr(ram_next_addr)

    //,.dbgled(LED1)
  );
  // /QSPI

  // LED blinker -----------------------------------------------------------------------
  wire [7:0]led_blinker_number;
  assign led_blinker_number = qspi_address[7:0];

  wire [2:0] demux_select;
  assign demux_select = qspi_command[2:0];

  wire [2:0] range_switch_select;
  assign range_switch_select = qspi_address[2:0];

  wire [4:0] flags;
  assign flags = qspi_command[7:3];

  wire number_blinker_trigger;
  wire dac_trigger;
  wire range_switch_trigger;

  demux8 cmd_demux(
    .i_sel(demux_select),
    .i_in(qspi_addr_rdy),
    .i_en(1'b1),
    .o_out4(range_switch_trigger),
    .o_out5(number_blinker_trigger),
    .o_out6(dac_trigger),
    .o_out7(adc_trigger)
  );

  /*
  latch #(.DURATION(10000000)) l (
    .i_clk(clk_main),
    .i_rst(rst),
    .i_trigger(qspi_addr_rdy),
    .o_signal(LED1)
  );
  */


  /*
  always @(posedge clk_main) begin
    if (qspi_addr_rdy == 1'b1) begin
      led <= 1'b1;
    end
  end
  */

  number_blinker #(.PERIOD(5000000), .GAP_PERIOD(20000000)) nb (
    .i_number(led_blinker_number),
    .i_clk(clk_main),
    .i_rst(rst),
    .i_trigger(number_blinker_trigger),
    .o_signal(LED1)
  );

  spi_dac dac (
    .i_clk(clk_main),
    .i_data(qspi_data[15:0]),
    .i_trigger(dac_trigger),
    .ncs(DAC_NCS),
    .sdi(DAC_SDI),
    .sck(DAC_SCK)
  );

  range_switch range_switch (
    .i_range(range_switch_select),
    .i_trigger(range_switch_trigger),
    .i_clk(clk_main),
    .o_meas_switch_0(MEAS_SWITCH_0),
    .o_meas_switch_1(MEAS_SWITCH_1),
    .o_meas_switch_2(MEAS_SWITCH_2),
    .o_range_sel_0(RANGE_SEL_0),
    .o_range_sel_1(RANGE_SEL_1),
    .o_range_sel_2(RANGE_SEL_2),
    .o_range_sel_3(RANGE_SEL_3),
    .o_range_sel_4(RANGE_SEL_4),
    .o_range_sel_5(RANGE_SEL_5)
  );



  // -----------------------------------------------------------------------------
  // qspi RDY signals are not in sync with the main clock so we just set
  // a flag on the rising edge of the signal and handle the state machine
  // in the main clock domain
  
  /*
  reg qspi_cmd_rdy_triggered;
  reg qspi_addr_rdy_triggered;
  reg qspi_data_rdy_triggered;

  synchronizer s_qspi_cmd_rdy (
    .async_signal(qspi_cmd_rdy),
    .clk(clk_main),
    .rising_edge(qspi_cmd_rdy_triggered)
  );

  synchronizer s_qspi_addr_rdy (
    .async_signal(qspi_addr_rdy),
    .clk(clk_main),
    .rising_edge(qspi_addr_rdy_triggered)
  );

  synchronizer s_qspi_data_rdy (
    .async_signal(qspi_data_rdy),
    .clk(clk_main),
    .rising_edge(qspi_data_rdy_triggered)
  );

  reg qspi_ncs_triggered;

  synchronizer s_qspi_ncs (
    .async_signal(~QSPI_NCS),
    .clk(clk_main),
    .rising_edge(qspi_ncs_triggered)
  );

  latch #(.DURATION(30000000)) l (
  //latch l (
    .i_clk(clk_main),
    .i_rst(rst),
    .i_trigger(qspi_ncs_triggered),
    .o_signal(led)
  );
  */



  // state machine
  // -----------------------------------------------------------------------------
  /*
  always @(posedge clk_main) begin
    led_blinker_trigger <= 1'b0;
    qspi_cmd_rdy_triggered <= 1'b0;
    qspi_addr_rdy_triggered <= 1'b0;
    qspi_data_rdy_triggered <= 1'b0;

    if (qspi_cmd_rdy_triggered == 1'b1) begin
      if (qspi_state == QSPI_IDLE) begin
        qspi_state <= QSPI_CMD_READ;
      end

    end else if (qspi_addr_rdy_triggered == 1'b1) begin
      if (qspi_state == QSPI_CMD_READ) begin
        if (qspi_command == 8'hAD) begin
          led_blinker_number <= qspi_address[3:0];
          led_blinker_trigger <= 1'b1;
        end
        qspi_state <= QSPI_IDLE;
      end

    end else if (qspi_data_rdy_triggered == 1'b1) begin
      // TODO
    end

  end
  */




endmodule
