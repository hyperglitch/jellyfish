// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module adc_ctrl
(
  input i_rst,
  input i_clk,
  output reg o_ncs1,
  output reg o_ncs2,
  output reg o_sdi,
  input i_sdo0,
  input i_sdo1,
  input i_sdo2,
  input i_sdo3,
  input i_sdo4,
  input i_sdo5,
  input i_sdo6,
  input i_sdo7,
  output reg o_sck,
  output reg o_nrst,
  output reg o_convst,
  input i_rdy1,
  input i_rdy2,
  output reg o_mux_sw,

  input i_trigger,
  output reg [15:0] o_data,
  output reg o_data_ready
);

  assign o_ncs1 = 1'b1;
  assign o_ncs2 = 1'b1;

  assign o_nrst = 1'b1; // power down

  reg trigger_old = 1'b0;

  reg rdy1_old = 1'b0;
  reg rdy2_old = 1'b0;

  localparam [1:0] IDLE = 2'd0;
  localparam [1:0] RDY_WAIT = 2'd1;
  localparam [1:0] DATA_READ = 2'd2;

  reg [1:0] current_state = IDLE;
  reg [4:0] data_counter = 0;

  always @(posedge i_clk or posedge i_rst) begin
    if (i_rst) begin
      o_ncs1 <= 1'b1;
      o_ncs2 <= 1'b1;
      o_sdi <= 1'b0;
      o_sck <= 1'b0;
      o_convst <= 1'b0;
      o_mux_sw <= 1'b0;
      o_nrst <= 1'b0;

      o_data_ready <= 1'b0;
      o_data <= 16'h0000;

      data_counter <= 0;

      current_state <= IDLE;

    end else begin
      o_nrst <= 1'b1; // release the reset pin

      case (current_state)
        IDLE: begin

          if (i_trigger == 1'b1 && trigger_old == 1'b0) begin
            // got trigger, start conversion
            o_convst <= 1'b1;
            current_state <= RDY_WAIT;
          end else begin
            o_convst <= 1'b0;
          end

        end
        RDY_WAIT: begin

          if (i_rdy1 == 1'b1 && rdy1_old == 1'b0) begin
            // data ready, drop NCS and start reading
            o_ncs1 <= 1'b0;
            current_state <= DATA_READ;

          end


        end
        DATA_READ: begin
          // toggle clock and read the data


        end
      endcase


      trigger_old <= i_trigger;
      rdy1_old <= i_rdy1;
      rdy2_old <= i_rdy2;


    end
  end



endmodule


