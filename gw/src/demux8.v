// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module demux8
(
  input [2:0] i_sel,
  input i_in,
  input i_en,
  output reg o_out0,
  output reg o_out1,
  output reg o_out2,
  output reg o_out3,
  output reg o_out4,
  output reg o_out5,
  output reg o_out6,
  output reg o_out7
);

  assign o_out0 = !i_sel[2] & !i_sel[1] & !i_sel[0] & i_en ? i_in : 1'b0;
  assign o_out1 = !i_sel[2] & !i_sel[1] &  i_sel[0] & i_en ? i_in : 1'b0;
  assign o_out2 = !i_sel[2] &  i_sel[1] & !i_sel[0] & i_en ? i_in : 1'b0;
  assign o_out3 = !i_sel[2] &  i_sel[1] &  i_sel[0] & i_en ? i_in : 1'b0;
  assign o_out4 =  i_sel[2] & !i_sel[1] & !i_sel[0] & i_en ? i_in : 1'b0;
  assign o_out5 =  i_sel[2] & !i_sel[1] &  i_sel[0] & i_en ? i_in : 1'b0;
  assign o_out6 =  i_sel[2] &  i_sel[1] & !i_sel[0] & i_en ? i_in : 1'b0;
  assign o_out7 =  i_sel[2] &  i_sel[1] &  i_sel[0] & i_en ? i_in : 1'b0;

endmodule

