// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module top(
  input CLK,
  output LED1,

  // used as data input
  input QSPI_BK1_IO0,
  input QSPI_BK1_IO1,
  input QSPI_BK1_IO2,
  input QSPI_BK1_IO3,
  input QSPI_BK2_IO0,
  input QSPI_BK2_IO1,
  input QSPI_BK2_IO2,
  input QSPI_BK2_IO3,

  // used as trigger input
  input QSPI_NCS,

  output DAC_NCS,
  output DAC_SDI,
  output DAC_SCK
);
  // initial reset signal after power on
  localparam RESET_DURATION_TICKS = 16'd2;

  // power on reset
  reg [15:0] reset_counter = RESET_DURATION_TICKS;
  wire rst = (reset_counter != 16'd0);
  always @(posedge CLK) begin
    if (reset_counter != 16'd0) begin
      reset_counter <= reset_counter - 16'd1;
    end
  end

  wire [15:0] data;
  assign data = {
    QSPI_BK1_IO3, QSPI_BK1_IO2, QSPI_BK1_IO1, QSPI_BK1_IO0,
    QSPI_BK1_IO3, QSPI_BK1_IO2, QSPI_BK1_IO1, QSPI_BK1_IO0,
    QSPI_BK2_IO3, QSPI_BK2_IO2, QSPI_BK2_IO1, QSPI_BK2_IO0,
    QSPI_BK2_IO3, QSPI_BK2_IO2, QSPI_BK2_IO1, QSPI_BK2_IO0
    };

  reg [15:0] r_data;
  reg r_trigger;

  always @(posedge CLK or posedge rst) begin
    if (rst) begin
      r_data <= 0;
      r_trigger <= 1'b0;
    end else begin
      r_data <= data;
      r_trigger <= QSPI_NCS;
    end
  end

  spi_dac dac (
    .i_clk(CLK),
    .i_data(r_data),
    .i_trigger(r_trigger),
    .ncs(DAC_NCS),
    .sdi(DAC_SDI),
    .sck(DAC_SCK)
  );

endmodule

