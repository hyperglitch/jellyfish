# JellyfishOPP FPGA code

This directory contains the Verilog code for the JellyfishOPP. The code is still under development. Currently the code includes the basic functionality verification of th JellyfishOPP peripherals.


### Prerequisites

The open source toolchain is required to build the code. The toolchain can be downloaded from [https://github.com/YosysHQ/oss-cad-suite-build](https://github.com/YosysHQ/oss-cad-suite-build).


### Building the code

```
make BOARD=jellyfish
```

### Running the tests

Tests are currently broken due to the bug in the implementation of the behavioral model of ICE40 specific modules (SB_IO, SB_RAM40_4K)

```
make BOARD=jellyfish test-<test_name>
```

### Programming the FPGA

```
make BOARD=jellyfish prog
```

## License

Verilog code is licensed under the GPLv3 license.
