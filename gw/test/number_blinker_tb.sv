`timescale 1ns / 1ps

// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module number_blinker_tb;

  localparam PERIOD = 2;

  reg [7:0] i_number;
  reg i_clk;
  reg i_rst;
  reg i_trigger;
  wire o_signal;

  number_blinker #(
    .PERIOD(PERIOD),
    .GAP_PERIOD(15)
    ) nb (
    .i_number(i_number),
    .i_clk(i_clk),
    .i_rst(i_rst),
    .i_trigger(i_trigger),
    .o_signal(o_signal)
  );

  // generate a clock
  initial begin
    i_clk = 0;
    forever #5 i_clk = ~i_clk; // 10ns clock period, 100MHz
  end

  // generate a test sequence
  initial begin
    i_rst = 1;  // reset the number blinker
    i_number = 8'd3;

    #10;
    i_rst = 0;  // release reset
    i_trigger = 1;
    #10
    i_trigger = 0;

    #2000;  // allow the simulation to run for some time
    i_number = 8'd0;
    i_trigger = 1;
    #10
    i_trigger = 0;

    #2000;
    i_number = 8'd1;

    #2000;
    i_number = 8'd10;
    i_trigger = 1;
    #10
    i_trigger = 0;

    #2000;

    $finish;
  end

  initial begin
    $dumpfile("build/number_blinker_tb.vcd");
    $dumpvars(0, number_blinker_tb);
  end

endmodule

