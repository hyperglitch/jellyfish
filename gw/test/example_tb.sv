`timescale 1ns/1ps

// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module example_tb;
    reg clk, rst;
    reg [7:0] data_in;
    wire [7:0] data_out;

    // Generate clock
    always #5 clk = ~clk;

    initial begin
        // Initialize signals
        clk = 0;
        rst = 1;
        data_in = 8'h00;

        // Reset pulse
        #10 rst = 0;

        // Test stimulus
        #20 data_in = 8'hA5;
        #20 data_in = 8'h5A;

        // Finish simulation
        #100 $finish;
    end

    initial begin
        $dumpfile("build/example_tb.vcd"); // Save waveforms
        $dumpvars(0, example_tb);
    end

endmodule
