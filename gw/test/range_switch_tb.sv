`timescale 1ns / 1ps

// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module range_switch_tb;

  reg [2:0] i_range;
  reg i_trigger = 1'b0;
  reg clk;
  wire o_meas_switch_0;
  wire o_meas_switch_1;
  wire o_meas_switch_2;
  wire o_range_sel_0;
  wire o_range_sel_1;
  wire o_range_sel_2;
  wire o_range_sel_3;
  wire o_range_sel_4;
  wire o_range_sel_5;

  initial begin
    clk = 0;
    forever begin
      #10 clk = ~clk;
    end
  end

  range_switch uut(
    .i_range(i_range),
    .i_trigger(i_trigger),
    .i_clk(clk),
    .o_meas_switch_0(o_meas_switch_0),
    .o_meas_switch_1(o_meas_switch_1),
    .o_meas_switch_2(o_meas_switch_2),
    .o_range_sel_0(o_range_sel_0),
    .o_range_sel_1(o_range_sel_1),
    .o_range_sel_2(o_range_sel_2),
    .o_range_sel_3(o_range_sel_3),
    .o_range_sel_4(o_range_sel_4),
    .o_range_sel_5(o_range_sel_5)
  );

  // generate a test sequence
  initial begin
    #30
    i_range = 3'd0;
    #5
    i_trigger = 1'b1;
    #20
    i_trigger = 1'b0;

    #30
    i_range = 3'd1;
    #5
    i_trigger = 1'b1;
    #20
    i_trigger = 1'b0;

    #30
    i_range = 3'd2;
    #5
    i_trigger = 1'b1;
    #20
    i_trigger = 1'b0;

    #30
    i_range = 3'd3;
    #5
    i_trigger = 1'b1;
    #20
    i_trigger = 1'b0;

    #30
    i_range = 3'd4;
    #5
    i_trigger = 1'b1;
    #20
    i_trigger = 1'b0;

    #30
    i_range = 3'd5;
    #5
    i_trigger = 1'b1;
    #20
    i_trigger = 1'b0;

    #30
    i_range = 3'd0;
    #5
    i_trigger = 1'b1;
    #20
    i_trigger = 1'b0;

    
    $finish;
  end

  initial begin
    $dumpfile("build/range_switch_tb.vcd");
    $dumpvars(0, range_switch_tb);
  end

endmodule

