`timescale 1ns / 1ps

// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module top_tb;

  reg i_clk = 0;
  wire led;

  reg qspi_clk;
  reg i_ncs;
  reg i_bk1_io0;
  reg i_bk1_io1;
  reg i_bk1_io2;
  reg i_bk1_io3;
  reg i_bk2_io0;
  reg i_bk2_io1;
  reg i_bk2_io2;
  reg i_bk2_io3;

  reg dac_ncs;
  reg dac_sdi;
  reg dac_sck;

  reg meas_switch_0;
  reg meas_switch_1;
  reg meas_switch_2;
  reg range_sel_0;
  reg range_sel_1;
  reg range_sel_2;
  reg range_sel_3;
  reg range_sel_4;
  reg range_sel_5;

  top dut (
    .CLK(i_clk),
    .LED1(led),

    .QSPI_CLK(qspi_clk),
    .QSPI_NCS(i_ncs),
    .QSPI_BK1_IO0(i_bk1_io0),
    .QSPI_BK1_IO1(i_bk1_io1),
    .QSPI_BK1_IO2(i_bk1_io2),
    .QSPI_BK1_IO3(i_bk1_io3),
    .QSPI_BK2_IO0(i_bk2_io0),
    .QSPI_BK2_IO1(i_bk2_io1),
    .QSPI_BK2_IO2(i_bk2_io2),
    .QSPI_BK2_IO3(i_bk2_io3),

    .DAC_NCS(dac_ncs),
    .DAC_SDI(dac_sdi),
    .DAC_SCK(dac_sck),

    .MEAS_SWITCH_0(meas_switch_0),
    .MEAS_SWITCH_1(meas_switch_1),
    .MEAS_SWITCH_2(meas_switch_2),
    .RANGE_SEL_0(range_sel_0),
    .RANGE_SEL_1(range_sel_1),
    .RANGE_SEL_2(range_sel_2),
    .RANGE_SEL_3(range_sel_3),
    .RANGE_SEL_4(range_sel_4),
    .RANGE_SEL_5(range_sel_5)
  );

  initial begin
    #3;
    i_clk = 0;
    forever #7 i_clk = ~i_clk; // slow main clock
  end

  initial begin
    qspi_clk = 0;
    #105;
    for(integer i=0; i<14; i=i+1) begin
      #5 qspi_clk = (~qspi_clk);
      #5 qspi_clk = (~qspi_clk);
    end

    #315;
    for(integer i=0; i<14; i=i+1) begin
      #5 qspi_clk = (~qspi_clk);
      #5 qspi_clk = (~qspi_clk);
    end
  end

  initial begin
    i_ncs = 1;
    i_bk1_io0 = 0;
    i_bk1_io1 = 0;
    i_bk1_io2 = 0;
    i_bk1_io3 = 0;
    i_bk2_io0 = 0;
    i_bk2_io1 = 0;
    i_bk2_io2 = 0;
    i_bk2_io3 = 0;

    #100;

    // chip select
    i_ncs = 0;
    #5;

    // send command over io0 line
    i_bk1_io0 = 1;
    i_bk2_io0 = 1;
    #10;

    i_bk1_io0 = 0;
    i_bk2_io0 = 0;
    #10;

    i_bk1_io0 = 1;
    i_bk2_io0 = 1;
    #10;

    i_bk1_io0 = 0;
    i_bk2_io0 = 0;
    #10;

    i_bk1_io0 = 1;
    i_bk2_io0 = 1;
    #10;
    #10;

    i_bk1_io0 = 0;
    i_bk2_io0 = 0;
    #10;

    i_bk1_io0 = 1;
    i_bk2_io0 = 1;
    #10;

    // send address over all four lines
    i_bk1_io0 = 0;
    i_bk1_io1 = 0;
    i_bk1_io2 = 0;
    i_bk1_io3 = 0;

    i_bk2_io0 = 0;
    i_bk2_io1 = 0;
    i_bk2_io2 = 0;
    i_bk2_io3 = 0;
    #10;

    i_bk1_io0 = 0;
    i_bk1_io1 = 0;
    i_bk1_io2 = 0;
    i_bk1_io3 = 0;

    i_bk2_io0 = 0;
    i_bk2_io1 = 0;
    i_bk2_io2 = 0;
    i_bk2_io3 = 0;
    #10;

    i_bk1_io0 = 1;
    i_bk1_io1 = 0;
    i_bk1_io2 = 1;
    i_bk1_io3 = 0;

    i_bk2_io0 = 1;
    i_bk2_io1 = 0;
    i_bk2_io2 = 1;
    i_bk2_io3 = 0;
    #10;

    i_bk1_io0 = 0;
    i_bk1_io1 = 1;
    i_bk1_io2 = 1;
    i_bk1_io3 = 0;

    i_bk2_io0 = 0;
    i_bk2_io1 = 1;
    i_bk2_io2 = 1;
    i_bk2_io3 = 0;
    #10;

    i_bk1_io0 = 0;
    i_bk1_io1 = 1;
    i_bk1_io2 = 0;
    i_bk1_io3 = 1;

    i_bk2_io0 = 0;
    i_bk2_io1 = 1;
    i_bk2_io2 = 0;
    i_bk2_io3 = 1;
    #10;

    i_bk1_io0 = 0;
    i_bk1_io1 = 1;
    i_bk1_io2 = 0;
    i_bk1_io3 = 1;

    i_bk2_io0 = 0;
    i_bk2_io1 = 1;
    i_bk2_io2 = 0;
    i_bk2_io3 = 1;
    #10;

    // dummy cycles
    i_bk1_io0 = 0;
    i_bk1_io1 = 0;
    i_bk1_io2 = 0;
    i_bk1_io3 = 0;

    i_bk2_io0 = 0;
    i_bk2_io1 = 0;
    i_bk2_io2 = 0;
    i_bk2_io3 = 0;
    #10;
    i_ncs = 1;

    #300;

    // another run
    // chip select
    i_ncs = 0;
    #5;

    // send command over io0 line
    i_bk1_io0 = 1;
    i_bk2_io0 = 1;
    #10;

    i_bk1_io0 = 0;
    i_bk2_io0 = 0;
    #10;

    i_bk1_io0 = 1;
    i_bk2_io0 = 1;
    #10;

    i_bk1_io0 = 0;
    i_bk2_io0 = 0;
    #10;

    i_bk1_io0 = 1;
    i_bk2_io0 = 1;
    #10;
    #10;

    i_bk1_io0 = 0;
    i_bk2_io0 = 0;
    #10;

    i_bk1_io0 = 1;
    i_bk2_io0 = 1;
    #10;

    // send address over all four lines
    i_bk1_io0 = 0;
    i_bk1_io1 = 0;
    i_bk1_io2 = 0;
    i_bk1_io3 = 0;

    i_bk2_io0 = 0;
    i_bk2_io1 = 0;
    i_bk2_io2 = 0;
    i_bk2_io3 = 0;
    #10;

    i_bk1_io0 = 0;
    i_bk1_io1 = 0;
    i_bk1_io2 = 0;
    i_bk1_io3 = 0;

    i_bk2_io0 = 0;
    i_bk2_io1 = 0;
    i_bk2_io2 = 0;
    i_bk2_io3 = 0;
    #10;

    i_bk1_io0 = 1;
    i_bk1_io1 = 0;
    i_bk1_io2 = 1;
    i_bk1_io3 = 0;

    i_bk2_io0 = 1;
    i_bk2_io1 = 0;
    i_bk2_io2 = 1;
    i_bk2_io3 = 0;
    #10;

    i_bk1_io0 = 0;
    i_bk1_io1 = 1;
    i_bk1_io2 = 1;
    i_bk1_io3 = 0;

    i_bk2_io0 = 0;
    i_bk2_io1 = 1;
    i_bk2_io2 = 1;
    i_bk2_io3 = 0;
    #10;

    i_bk1_io0 = 0;
    i_bk1_io1 = 1;
    i_bk1_io2 = 0;
    i_bk1_io3 = 1;

    i_bk2_io0 = 0;
    i_bk2_io1 = 1;
    i_bk2_io2 = 0;
    i_bk2_io3 = 1;
    #10;

    i_bk1_io0 = 0;
    i_bk1_io1 = 1;
    i_bk1_io2 = 0;
    i_bk1_io3 = 1;

    i_bk2_io0 = 0;
    i_bk2_io1 = 1;
    i_bk2_io2 = 0;
    i_bk2_io3 = 1;
    #10;

    // dummy cycles
    i_bk1_io0 = 0;
    i_bk1_io1 = 0;
    i_bk1_io2 = 0;
    i_bk1_io3 = 0;

    i_bk2_io0 = 0;
    i_bk2_io1 = 0;
    i_bk2_io2 = 0;
    i_bk2_io3 = 0;
    #10;
    i_ncs = 1;
    #300;


    $finish;
  end

  initial begin
    $dumpfile("build/top_tb.vcd");
    $dumpvars(0, top_tb);
  end


endmodule
