`timescale 1ns / 1ps

// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module clock_divider_tb;

  reg i_rst;
  reg i_clk;
  wire o_clk16, o_clk4, o_clk2, o_clk1;

  // instantiate the clock divider with a divisor of 64
  clock_divider #(.DIVISOR(16)) cd16 (
    .i_rst(i_rst),
    .i_clk(i_clk),
    .o_clk(o_clk16)
  );

  // instantiate the clock divider with a divisor of 4
  clock_divider #(.DIVISOR(4)) cd4 (
    .i_rst(i_rst),
    .i_clk(i_clk),
    .o_clk(o_clk4)
  );

  // instantiate the clock divider with a divisor of 2
  clock_divider #(.DIVISOR(2)) cd2 (
    .i_rst(i_rst),
    .i_clk(i_clk),
    .o_clk(o_clk2)
  );

  // instantiate the clock divider with a divisor of 1
  clock_divider #(.DIVISOR(1)) cd1 (
    .i_rst(i_rst),
    .i_clk(i_clk),
    .o_clk(o_clk1)
  );

  // generate a clock
  initial begin
    i_clk = 0;
    forever #5 i_clk = ~i_clk; // 10ns clock period, 100MHz
  end

  // generate a test sequence
  initial begin

    i_rst = 1;  // reset the dividers
    #10;
    i_rst = 0;  // release reset

    #300;  // allow the simulation to run for some time

    $finish;
  end

  initial begin
    $dumpfile("build/clock_divider_tb.vcd");
    $dumpvars(0, clock_divider_tb);
  end

endmodule
