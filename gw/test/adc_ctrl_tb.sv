`timescale 1ns/1ps

// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module adc_ctrl_tb;

  reg i_rst;
  reg i_clk;
  wire o_ncs1;
  wire o_ncs2;
  wire o_sdi;
  reg i_sdo0;
  reg i_sdo1;
  reg i_sdo2;
  reg i_sdo3;
  reg i_sdo4;
  reg i_sdo5;
  reg i_sdo6;
  reg i_sdo7;
  wire o_sck;
  wire o_nrst;
  wire o_convst;
  reg i_rdy1;
  reg i_rdy2;
  wire o_mux_sw;

  reg i_trigger;
  wire [15:0] o_data;
  wire o_data_ready;

  adc_ctrl adc (
    .i_rst(i_rst),
    .i_clk(i_clk),
    .o_ncs1(o_ncs1),
    .o_ncs2(o_ncs2),
    .o_sdi(o_sdi),
    .i_sdo0(i_sdo0),
    .i_sdo1(i_sdo1),
    .i_sdo2(i_sdo2),
    .i_sdo3(i_sdo3),
    .i_sdo4(i_sdo4),
    .i_sdo5(i_sdo5),
    .i_sdo6(i_sdo6),
    .i_sdo7(i_sdo7),
    .o_sck(o_sck),
    .o_nrst(o_nrst),
    .o_convst(o_convst),
    .i_rdy1(i_rdy1),
    .i_rdy2(i_rdy2),
    .o_mux_sw(o_mux_sw),
    .i_trigger(i_trigger),
    .o_data(o_data),
    .o_data_ready(o_data_ready)
  );

  // generate a clock
  initial begin
    i_clk = 0;
    forever #5 i_clk = ~i_clk; // 10ns clock period, 100MHz
  end

  // generate a test sequence
  initial begin
    i_rst = 1;  // reset the number blinker

    #10;
    i_rst = 0;  // release reset
    i_trigger = 1;
    #10
    i_trigger = 0;

    #2000;  // allow the simulation to run for some time

    $finish;
  end

  initial begin
    $dumpfile("build/adc_ctrl_tb.vcd");
    $dumpvars(0, adc_ctrl_tb);
  end

endmodule
