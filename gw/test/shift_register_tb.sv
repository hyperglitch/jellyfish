`timescale 1ns / 1ps

// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module shift_register_tb;

  localparam WIDTH = 8;
  localparam SHIFT_LEFT = 1;

  reg i_clk;
  reg i_rst;
  reg i_input;
  wire [WIDTH-1:0] o_data;

  shift_register #(.WIDTH(WIDTH), .SHIFT_LEFT(SHIFT_LEFT)) sr (
    .i_clk(i_clk),
    .i_rst(i_rst),
    .i_input(i_input),
    .o_data(o_data)
  );

  // generate a clock
  initial begin
    i_clk = 0;
    forever #5 i_clk = ~i_clk; // 10ns clock period, 100MHz
  end

  // generate a test sequence
  initial begin
    i_rst = 1;  // reset the shift register
    i_input = 0;
    #10;
    i_rst = 0;  // release reset

    #10 i_input = 1;
    #10 i_input = 1;
    #10 i_input = 1;
    #10 i_input = 0;
    #10 i_input = 0;
    #10 i_input = 1;
    #100;
    i_input = 0;
    #100;
    $finish;
  end

  initial begin
    $dumpfile("build/shift_register_tb.vcd");
    $dumpvars(0, shift_register_tb);
  end

endmodule
