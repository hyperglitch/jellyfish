`timescale 1ns / 1ps

// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module demux8_tb;

  reg [2:0] i_sel;
  reg i_in;
  reg i_en;
  wire o_out0;
  wire o_out1;
  wire o_out2;
  wire o_out3;
  wire o_out4;
  wire o_out5;
  wire o_out6;
  wire o_out7;

  demux8 uut(
    .i_sel(i_sel),
    .i_in(i_in),
    .i_en(i_en),
    .o_out0(o_out0),
    .o_out1(o_out1),
    .o_out2(o_out2),
    .o_out3(o_out3),
    .o_out4(o_out4),
    .o_out5(o_out5),
    .o_out6(o_out6),
    .o_out7(o_out7)
  );

  // generate a clock
  initial begin
    i_in = 0;
    forever #5 i_in = ~i_in; // 10ns clock period, 100MHz
  end

  // generate a test sequence
  initial begin
    i_en = 1;
    i_sel = 3'b000;
    #30
    i_sel = 3'b001;
    #30
    i_sel = 3'b010;
    #30
    i_sel = 3'b011;
    #30
    i_sel = 3'b100;
    #30
    i_sel = 3'b101;
    #30
    i_sel = 3'b110;
    #30
    i_sel = 3'b111;
    #30
    i_en = 0;
    i_sel = 3'b000;
    #30
    i_sel = 3'b001;
    #30
    i_sel = 3'b010;
    #30
    #30

    $finish;
  end

  initial begin
    $dumpfile("build/demux8_tb.vcd");
    $dumpvars(0, demux8_tb);
  end

endmodule
