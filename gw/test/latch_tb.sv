`timescale 1ns/1ps

// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module latch_tb;

  reg clk;
  reg rst;
  reg trigger;

  wire signal;

  latch #(.DURATION(10)) l (
    .i_clk(clk),
    .i_rst(rst),
    .i_trigger(trigger),
    .o_signal(signal)
  );

  initial begin
    clk = 0;
    forever begin
      #10 clk = ~clk;
    end
  end

  initial begin
    clk = 0;
    rst = 1;
    trigger = 0;
    #10;
    rst = 0;
    #20;
    trigger = 1;
    #10;
    trigger = 0;
    #300;
    trigger = 1;
    #10;
    trigger = 0;
    #300;

    $finish;
  end


  initial begin
    $dumpfile("build/latch_tb.vcd");
    $dumpvars(0, latch_tb);
  end

endmodule

