// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module SB_RAM40_4K
#(
  parameter integer INIT_0 = 256'h0,
  parameter integer INIT_1 = 256'h0,
  parameter integer INIT_2 = 256'h0,
  parameter integer INIT_3 = 256'h0,
  parameter integer INIT_4 = 256'h0,
  parameter integer INIT_5 = 256'h0,
  parameter integer INIT_6 = 256'h0,
  parameter integer INIT_7 = 256'h0
)(
    output reg [15:0] RDATA,
    input wire [10:0] RADDR,
    input wire RCLK,
    input wire RCLKE,
    input wire RE,
    input wire [10:0] WADDR,
    input wire WCLK,
    input wire WCLKE,
    input wire [15:0] WDATA,
    input wire WE,
    input wire [15:0] MASK
);

    // 4096-bit memory (256x16)
    reg [15:0] memory [0:255];

    // Simple initialization (optional)
    initial begin
        memory[0]  = 16'h0123;
        memory[1]  = 16'h4567;
        memory[2]  = 16'h89AB;
        memory[3]  = 16'hCDEF;
        memory[4]  = 16'h1111;
        memory[5]  = 16'h2222;
        memory[6]  = 16'h3333;
        memory[7]  = 16'h4444;
        // Add more as needed...
    end

    // Read logic (synchronous)
    always @(posedge RCLK) begin
        if (RCLKE && RE)
            RDATA <= memory[RADDR[7:0]]; // Only 256 words are modeled
    end

    // Write logic (synchronous)
    always @(posedge WCLK) begin
        if (WCLKE && WE)
            memory[WADDR[7:0]] <= (WDATA & ~MASK) | (memory[WADDR[7:0]] & MASK);
    end

endmodule

module SB_IO #(
    parameter PIN_TYPE = 6'b000000, // Default: input mode
    parameter PULLUP = 1'b0         // Optional pull-up resistor
) (
    inout wire PACKAGE_PIN,  // External FPGA pin
    input wire OUTPUT_ENABLE, // Enables output
    input wire D_OUT_0,      // Output data
    input wire D_OUT_1,      // Output data for DDR mode (ignored in simple cases)
    output reg D_IN_0,       // Input data
    output reg D_IN_1        // Input data (used in DDR mode, ignored here)
);

    reg internal_value; // Stores output when enabled
    reg package_pullup; // Simulates pull-up resistor

    initial begin
        package_pullup = PULLUP;
    end

    always @(*) begin
        if (OUTPUT_ENABLE) begin
            internal_value = D_OUT_0;
        end else begin
            internal_value = 1'bz; // High impedance when not output
        end
    end

    // Assign bidirectional behavior
    assign PACKAGE_PIN = OUTPUT_ENABLE ? internal_value : 1'bz;

    // Simulate input behavior
    always @(*) begin
        if (!OUTPUT_ENABLE) begin
            D_IN_0 = (package_pullup && PACKAGE_PIN === 1'bz) ? 1'b1 : PACKAGE_PIN;
            D_IN_1 = D_IN_0; // DDR input (same as D_IN_0 in simple case)
        end
    end

endmodule

