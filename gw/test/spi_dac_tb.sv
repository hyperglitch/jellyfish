`timescale 1ns / 1ps

// SPDX-FileCopyrightText: 2025 Igor Brkic <igor@hyperglitch.com>
// SPDX-License-Identifier: GPL-3.0-or-later

module spi_dac_tb;

  reg [15:0] i_data;
  reg i_trigger;
  reg i_clk;
  wire ncs;
  wire sdi;
  wire sck;

  spi_dac nb (
    .i_data(i_data),
    .i_clk(i_clk),
    .i_trigger(i_trigger),
    .ncs(ncs),
    .sdi(sdi),
    .sck(sck)
  );

  // generate a clock
  initial begin
    i_clk = 0;
    forever #5 i_clk = ~i_clk; // 10ns clock period, 100MHz
  end

  // generate a test sequence
  initial begin
    i_data = 16'hAD51;
    i_trigger = 0;
    #10 i_trigger = 1;
    #10 i_trigger = 0;
    #1000;
    i_data = 16'h8023;
    #10 i_trigger = 1;
    #10 i_trigger = 0;
    #1000;
    $finish;
  end

  initial begin
    $dumpfile("build/spi_dac_tb.vcd");
    $dumpvars(0, spi_dac_tb);
  end

endmodule

